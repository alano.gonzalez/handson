package com.softtek.academyjava.handson.HandsOn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Workbook;

import com.softtek.academyjava.handson.MySql.SqlConSingleton;

public class App 
{
    public static void main( String[] args ) throws SQLException
    {
    	String query = "select deviceId, name, description, manufacturerId, colorId, ifnull(comments, 'comentario') as comments "
           		+ "from Device;";
       SqlConSingleton singleton =  SqlConSingleton.getInstance("jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234");
       PreparedStatement state = singleton.getStatement(query);
       List<Device> resultList = singleton.makeQuery(query, state);
       singleton.closer();
      
       System.out.println("Nombres de dispositivos cuyo name es Laptop");
       List<String> mappedLaptops = resultList.stream()
       		.map(d -> d)
       		.filter(d -> d.getName().equals("Laptop"))
       		.map(d -> d.getDescription())
       		.collect(Collectors.toList());
       mappedLaptops.forEach(System.out::println);
       
       Long no_Id3  = resultList.stream()
    		   .map(d -> d.getDeviceId())
    		   .filter(d -> d.equals(3L))
    		   .count();
       System.out.println("Numero de Dispositivos con ID=3 : " + no_Id3);
       
       System.out.println("Dispositivos cuyo ColorId = 1: ");
       List<Device> listaDeviceId1 = resultList.stream()
    		   .filter(d -> d.getColorId().equals(1L))
    		   .collect(Collectors.toList());
       listaDeviceId1.forEach(System.out::println);
       System.out.println("Mapa de dispositivos con deviceId como llave y Device como valor");
       
       Map<Long, List<Device>> mapaDevice = resultList.stream()
    		   .collect(Collectors.groupingBy(Device::getDeviceId));
       mapaDevice.forEach((k,v) -> System.out.println("DeviceID: " + k + "    Device: " + v));
       
       
       
    }
}
