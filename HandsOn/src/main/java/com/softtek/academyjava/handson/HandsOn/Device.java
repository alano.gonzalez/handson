package com.softtek.academyjava.handson.HandsOn;

public class Device {

	private Long deviceId;
	private String Name;
	private String Description;
	private Long manufacturerId;
	private Long colorId;
	private String comments;
	
	
	
	public Device() {
	}



	public Device(Long deviceId, String name, String description, Long manufacturerId, Long colorId, String comments) {
		super();
		this.deviceId = deviceId;
		Name = name;
		Description = description;
		this.manufacturerId = manufacturerId;
		this.colorId = colorId;
		this.comments = comments;
	}



	public Long getDeviceId() {
		return deviceId;
	}



	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}



	public String getName() {
		return Name;
	}



	public void setName(String name) {
		Name = name;
	}



	public String getDescription() {
		return Description;
	}



	public void setDescription(String description) {
		Description = description;
	}



	public Long getManufacturerId() {
		return manufacturerId;
	}



	public void setManufacturerId(Long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}



	public Long getColorId() {
		return colorId;
	}



	public void setColorId(Long colorId) {
		this.colorId = colorId;
	}



	public String getComments() {
		return comments;
	}



	public void setComments(String comments) {
		this.comments = comments;
	}



	@Override
	public String toString() {
		return "Device -> [deviceId: " + deviceId + ", Name: " + Name + ", Description: " + Description + ", manufacturerId: "
				+ manufacturerId + ", colorId: " + colorId + ", comments: " + comments + "]";
	}
	
	
	
	
	
}
