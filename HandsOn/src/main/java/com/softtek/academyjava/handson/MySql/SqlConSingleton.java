package com.softtek.academyjava.handson.MySql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academyjava.handson.HandsOn.Device;


public  class SqlConSingleton {

	private Connection connection;
	private static SqlConSingleton uniqueInstance;
	private String user, password, driver;
	
	
	private SqlConSingleton(String driver, String user, String password) {
		super();
		this.user = user;
		this.password = password;
		this.driver = driver;
		
		try {
			this.connection = DriverManager.getConnection(this.driver, this.user, this.password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public static synchronized SqlConSingleton getInstance(String driver, String user, String pass) {
		if (uniqueInstance == null) {
			uniqueInstance = new SqlConSingleton(driver, user, pass);
		}
		return uniqueInstance;
	}
	
	public PreparedStatement getStatement(String query) throws SQLException {
		return this.connection.prepareStatement(query);
	}
	
	public void closer() throws SQLException {
		connection.close();
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		connection.close();
	}
	
	public  List<Device> makeQuery(String query, PreparedStatement st) throws SQLException {
		List<Device> resultList = new ArrayList<>();
		
		ResultSet resultSet = st.executeQuery();
		while(resultSet.next()) {
			Long deviceId = resultSet.getLong("deviceId");
			String name = resultSet.getString("name");
			String description = resultSet.getString("description");
			Long manufacturerId = resultSet.getLong("manufacturerId");
			Long colorId = resultSet.getLong("colorId");
			String comments = resultSet.getString("comments");
			
			resultList.add(new Device(deviceId, name, description, manufacturerId, colorId, comments));
		   }
		return resultList;
		
	}

	
		
}
	
	

